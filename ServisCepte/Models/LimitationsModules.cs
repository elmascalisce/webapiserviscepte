﻿using System;
using System.Collections.Generic;

namespace ServisCepte.Models
{
    public partial class LimitationsModules
    {
        public int Id { get; set; }
        public string Customer { get; set; }
        public string Stock { get; set; }
        public string Installment { get; set; }
        public string Offer { get; set; }
        public string Order { get; set; }
        public string Waybill { get; set; }
        public string Invoice { get; set; }
        public string Safe { get; set; }
        public string Pos { get; set; }
        public string Bank { get; set; }
        public string CheckCommercialPaper { get; set; }
        public string Document { get; set; }
        public string Department { get; set; }
        public string Targeting { get; set; }
        public string Creditcard { get; set; }
        public string Sms { get; set; }
        public string IncomeAndExpense { get; set; }
        public string Currency { get; set; }
        public string Vehicle { get; set; }
        public string ProducerReceipt { get; set; }
        public string Key { get; set; }
        public string Elevator { get; set; }
        public string AppKey { get; set; }
        public string Production { get; set; }
        public string FarmManagement { get; set; }
        public string Agricultural { get; set; }
        public string EInvoice { get; set; }
        public string Restaurant { get; set; }
        public int? Limitations { get; set; }

        public Limitations IdNavigation { get; set; }
    }
}
