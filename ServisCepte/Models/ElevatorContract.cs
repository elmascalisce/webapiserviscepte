﻿using System;
using System.Collections.Generic;

namespace ServisCepte.Models
{
    public partial class ElevatorContract
    {
        public int Id { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public int? CustomerId { get; set; }
        public decimal? ContractFee { get; set; }
        public string Notes { get; set; }
        public int? ElevatorCount { get; set; }
        public sbyte? Deletion { get; set; }
        public sbyte? Active { get; set; }

        public Customer Customer { get; set; }
    }
}
