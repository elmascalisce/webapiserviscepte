﻿using System;
using System.Collections.Generic;

namespace ServisCepte.Models
{
    public partial class ElevatorOfferTemp
    {
        public int Id { get; set; }
        public int? ElevatorType { get; set; }
        public int? CustomerId { get; set; }
        public string OfferNumber { get; set; }
        public DateTime? OfferDate { get; set; }
        public int? GroupCount { get; set; }
        public sbyte? IsSaved { get; set; }
        public int? QualityBasket { get; set; }
        public int? SizesBasket { get; set; }
        public string ElevatorTypeName { get; set; }
    }
}
