﻿using System;
using System.Collections.Generic;

namespace ServisCepte.Models
{
    public partial class ElevatorMaterialFormula
    {
        public ElevatorMaterialFormula()
        {
            ElevatorMaterialFormulaCalc = new HashSet<ElevatorMaterialFormulaCalc>();
        }

        public int Id { get; set; }
        public int? ElevatorMaterialFormulaDefinesId { get; set; }
        public string ProductName { get; set; }
        public int? ProductId { get; set; }
        public string ProductGroup { get; set; }
        public sbyte? ApplicationPart { get; set; }

        public ICollection<ElevatorMaterialFormulaCalc> ElevatorMaterialFormulaCalc { get; set; }
    }
}
