﻿using System;
using System.Collections.Generic;

namespace ServisCepte.Models
{
    public partial class ProducerReceiptDailyPurchases
    {
        public int Id { get; set; }
        public int? CustomerId { get; set; }
        public float? S1 { get; set; }
        public float? S2 { get; set; }
        public float? S3 { get; set; }
        public float? S4 { get; set; }
        public float? S5 { get; set; }
        public float? S6 { get; set; }
        public float? S7 { get; set; }
        public float? S8 { get; set; }
        public float? S9 { get; set; }
        public float? S10 { get; set; }
        public float? S11 { get; set; }
        public float? S12 { get; set; }
        public float? S13 { get; set; }
        public float? S14 { get; set; }
        public float? S15 { get; set; }
        public float? S16 { get; set; }
        public float? S17 { get; set; }
        public float? S18 { get; set; }
        public float? S19 { get; set; }
        public float? S20 { get; set; }
        public float? S21 { get; set; }
        public float? S22 { get; set; }
        public float? S23 { get; set; }
        public float? S24 { get; set; }
        public float? S25 { get; set; }
        public float? S26 { get; set; }
        public float? S27 { get; set; }
        public float? S28 { get; set; }
        public float? S29 { get; set; }
        public float? S30 { get; set; }
        public float? S31 { get; set; }
        public float? A1 { get; set; }
        public float? A2 { get; set; }
        public float? A3 { get; set; }
        public float? A4 { get; set; }
        public float? A5 { get; set; }
        public float? A6 { get; set; }
        public float? A7 { get; set; }
        public float? A8 { get; set; }
        public float? A9 { get; set; }
        public float? A10 { get; set; }
        public float? A11 { get; set; }
        public float? A12 { get; set; }
        public float? A13 { get; set; }
        public float? A14 { get; set; }
        public float? A15 { get; set; }
        public float? A16 { get; set; }
        public float? A17 { get; set; }
        public float? A18 { get; set; }
        public float? A19 { get; set; }
        public float? A20 { get; set; }
        public float? A21 { get; set; }
        public float? A22 { get; set; }
        public float? A23 { get; set; }
        public float? A24 { get; set; }
        public float? A25 { get; set; }
        public float? A26 { get; set; }
        public float? A27 { get; set; }
        public float? A28 { get; set; }
        public float? A29 { get; set; }
        public float? A30 { get; set; }
        public float? A31 { get; set; }
        public int? Period { get; set; }
        public sbyte? Stats { get; set; }
        public int? InvoiceId { get; set; }

        public Customer Customer { get; set; }
    }
}
