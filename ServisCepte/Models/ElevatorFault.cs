﻿using System;
using System.Collections.Generic;

namespace ServisCepte.Models
{
    public partial class ElevatorFault
    {
        public ElevatorFault()
        {
            ElevatorFaultHistory = new HashSet<ElevatorFaultHistory>();
        }

        public int Id { get; set; }
        public int? CustomerId { get; set; }
        public string AccountTitle { get; set; }
        public string Exp1 { get; set; }
        public string Exp2 { get; set; }
        public int? ReportUser { get; set; }
        public int? SolveUser { get; set; }
        public DateTime? ReportDatetime { get; set; }
        public DateTime? SolveDatetime { get; set; }
        public sbyte? Status { get; set; }
        public int? ElevatorFaultDefinesId { get; set; }
        public string ElevatorFaultDefinesFaultDefine { get; set; }
        public int? AcceptUserId { get; set; }
        public string Denouncing { get; set; }
        public string ReportPhone { get; set; }
        public int? ToUserId { get; set; }
        public string ToUserName { get; set; }
        public sbyte? IsGuard { get; set; }

        public Management AcceptUser { get; set; }
        public ElevatorFaultDefines ElevatorFaultDefines { get; set; }
        public ICollection<ElevatorFaultHistory> ElevatorFaultHistory { get; set; }
    }
}
