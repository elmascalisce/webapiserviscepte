﻿using System;
using System.Collections.Generic;

namespace ServisCepte.Models
{
    public partial class Invoice
    {
        public Invoice()
        {
            InvoiceProducerReceiptTaxList = new HashSet<InvoiceProducerReceiptTaxList>();
            InvoiceShipmentTrace = new HashSet<InvoiceShipmentTrace>();
        }

        public int Id { get; set; }
        public int? Type { get; set; }
        public int? InvoiceProductBasket { get; set; }
        public DateTime? DateTime { get; set; }
        public DateTime? InvoiceDateTime { get; set; }
        public int? UserId { get; set; }
        public int? CustomerId { get; set; }
        public string Barcode { get; set; }
        public decimal? InvoiceAmount { get; set; }
        public string InvoiceSerial { get; set; }
        public string InvoiceNumber { get; set; }
        public sbyte? SubCustomerDistrubite { get; set; }
        public sbyte? PayType { get; set; }
        public int? PayDetailId { get; set; }
        public int? PricingId { get; set; }
        public int? ExpiringId { get; set; }
        public sbyte? Stats { get; set; }
        public int? DetailId { get; set; }
        public sbyte? Deletion { get; set; }
        public int? IfEditNewInvoiceId { get; set; }
        public DateTime? DeletionDateTime { get; set; }
        public int? DeletionUserId { get; set; }
        public string PayDoor { get; set; }
        public string PayMethod { get; set; }
        public int? ShipmentStats { get; set; }
        public sbyte? CustomerCurrentStats { get; set; }
        public sbyte? MaintenanceInvoice { get; set; }

        public ICollection<InvoiceProducerReceiptTaxList> InvoiceProducerReceiptTaxList { get; set; }
        public ICollection<InvoiceShipmentTrace> InvoiceShipmentTrace { get; set; }
    }
}
