﻿using System;
using System.Collections.Generic;

namespace ServisCepte.Models
{
    public partial class CurrencyIn
    {
        public int Id { get; set; }
        public int? CurrencyId { get; set; }
    }
}
