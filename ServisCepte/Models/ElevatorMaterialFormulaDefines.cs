﻿using System;
using System.Collections.Generic;

namespace ServisCepte.Models
{
    public partial class ElevatorMaterialFormulaDefines
    {
        public int Id { get; set; }
        public int? ElevatorType { get; set; }
        public int? DriveType { get; set; }
        public int? SpeedType { get; set; }
        public int? CommandType { get; set; }
        public int? FoldDoors { get; set; }
        public int? CabinDoor { get; set; }
        public int? CoatingBooths { get; set; }
        public int? BoxType { get; set; }
        public int? MachineRoomLocation { get; set; }
        public string Exp1 { get; set; }
        public string Exp2 { get; set; }
        public string Defination { get; set; }
    }
}
