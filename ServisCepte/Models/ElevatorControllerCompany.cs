﻿using System;
using System.Collections.Generic;

namespace ServisCepte.Models
{
    public partial class ElevatorControllerCompany
    {
        public int Id { get; set; }
        public string CompanyName { get; set; }
    }
}
