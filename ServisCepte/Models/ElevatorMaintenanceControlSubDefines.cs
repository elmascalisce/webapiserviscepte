﻿using System;
using System.Collections.Generic;

namespace ServisCepte.Models
{
    public partial class ElevatorMaintenanceControlSubDefines
    {
        public int Id { get; set; }
        public sbyte? ControlType { get; set; }
        public int? OrderNumber { get; set; }
        public sbyte? IsImportant { get; set; }
        public string ControlDefine { get; set; }
        public sbyte Active { get; set; }
        public int? ElevatorMaintenanceControlDefinesId { get; set; }

        public ElevatorMaintenanceControlDefines ElevatorMaintenanceControlDefines { get; set; }
    }
}
