﻿using System;
using System.Collections.Generic;

namespace ServisCepte.Models
{
    public partial class ElevatorBaseInformation
    {
        public int Id { get; set; }
        public DateTime? AcceptanceDate { get; set; }
        public string RailUser { get; set; }
        public int? RailUserId { get; set; }
        public sbyte? Manufacturer { get; set; }
        public sbyte? Warranty { get; set; }
        public DateTime? DueDate { get; set; }
        public string InstallationUser { get; set; }
        public int? InstallationUserId { get; set; }
        public DateTime? RailDate { get; set; }
        public DateTime? InstallationDate { get; set; }
        public int? CustomerId { get; set; }
        public int? ElevatorOrder { get; set; }
        public string IdentificationNumber { get; set; }
        public string ElevatorCode { get; set; }
        public string ElevatorName { get; set; }
        public string Direction { get; set; }

        public Customer Customer { get; set; }
    }
}
