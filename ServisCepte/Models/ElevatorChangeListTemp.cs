﻿using System;
using System.Collections.Generic;

namespace ServisCepte.Models
{
    public partial class ElevatorChangeListTemp
    {
        public int Id { get; set; }
        public int? CustomerId { get; set; }
        public int? ProductId { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public sbyte? Type { get; set; }
        public int? UserId { get; set; }
        public int? Basket { get; set; }
        public DateTime? DateTime { get; set; }
        public string Floor { get; set; }
        public sbyte? ElevatorNumber { get; set; }
        public sbyte? InvoiceStats { get; set; }
        public int? Qty { get; set; }
        public decimal? UnitPriceTax { get; set; }
        public int? InvoiceId { get; set; }
    }
}
