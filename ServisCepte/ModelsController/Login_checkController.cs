﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ServisCepte.Attributes;
using ServisCepte.Models;
using ServisCepte.Models.PocoModels;

namespace ServisCepte.ModelsController
{
    [Route("api/[controller]")]
    [ApiController]
    public class Login_checkController : ControllerBase
    {

        private crocodileContext context;
        //public PocoContext poco;
        public Login_checkController(crocodileContext contex)
        {
            this.context = contex;
        }

        [BasicAuthorize("serviscepte")]
        [HttpGet]
        [Route("login_check")]
        public JsonResult Get()
        {
            return new JsonResult(PocoContext.list.ToList());
        }

    }
}