﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ServisCepte.Attributes;
using ServisCepte.Models;
using ServisCepte.Models.PocoModels;

namespace ServisCepte.ModelsController
{
    [Route("api/[controller]")]
    [ApiController]
    public class Service_requestController : ControllerBase
    {

        private crocodileContext context;
        //public PocoContext poco;
        public Service_requestController(crocodileContext contex)
        {
            this.context = contex;
        }

        [BasicAuthorize("serviscepte")]
        [HttpGet]
        [Route("service_request_list")]
        public JsonResult ServiceList()
        {
            PocoContext.Serviceslist = new List<ServiceRequestModel>();
            var list = context.ServicesRequestDefination.ToList();
            list.ForEach(k => PocoContext.Serviceslist.Add(new ServiceRequestModel(k)));
            return new JsonResult(PocoContext.Serviceslist.ToList());
        }

    }
}